
// These must be defined 
var HOST_IP = 'localhost';
var HOST_PORT = 9000;
var projector_unique_id = '123456ABC';

// var SERVER_URL = 'ws:/localhost:8000/ws/multi/'
var SERVER_URL = 'wss:/jims-av.ornear.com/ws/multi/'
//const token = '623afc041480ca5a898c38bf235906ca4c331e11'  //localhost
const token = '5e421468b8905871aaf699d4979de06c16d5f4a9'

// var uuid = 'c144a1a5-2747-441b-a734-dc68bf6d47ea';
var uuid = ''

var fs = require('fs');
fs.readFile('ltp_uuid.json', function (err, data) {
  if (err) return;
  uuid = JSON.parse(data).uuid;
  console.log("Found uuid: " + uuid);

});





// These will be populated
var user_id = 0;
var driver_id = 0;
var device_id = 0;

var power_talent = 0;
var mute_talent = 0;
var volume_talent = 0;

var current_request_id = 100;
var request_lookup = {'power': null, 'mute': null, 'volume': null}

var subscriptions = {'driverstream': new Set(), 'devicestream': new Set(), 'talentstream': new Set()}

var device_is_online = false;
var current_power = null;
var current_volume = 0;
var current_mute = null;
var last_command = null;
let mute_transistion_value = null;
var volume_transition_value = null;
let power_transistion_value = null;
var power_in_transition = false;
var mute_in_transition = false;
var volume_in_transition = false;

var intervalTCPConnect = false;
var intervalWSConnect = false;

//websocket

var WebSocket = require('ws')



// const ws = new WebSocket('ws://localhost:8000/ws/multi/', {
//     headers: {
//         'Authorization': 'Token ' + token
//    }
// });


//projector socket
var net = require('net');
var client = new net.Socket();

//connect to projector
connect();


function get_mute(){
    client.write('MUT???\r\n');
}

function get_vol(){
    client.write('VOL???\r\n');
}

function get_pow(){
    client.write('POW???\r\n');
}




const poll = setInterval(() => {
    get_pow();
    setTimeout(get_mute, 200);
    setTimeout(get_vol, 400);

    }, 2000);



client.on('data', function(data) {
    // console.log('Received: ' + data);
    // console.log('Current Volume: ' + current_volume)
    // console.log('Current Power: ' + current_power)
    // clearTCPInterval(intervalObj);
    data = data.toString().replace(/[/s]/g, '')//Remove /r/n
    var data_command = data.slice(0,3);  // Get first three letters
    var data_value = Number(data.slice(3));
    console.log(data_command + data_value)

    switch(data_command){
        case 'ERR':
            console.log(data);
            break;

        case 'ACK':
            // Acknowledgement
            // For all acknowledged commands we need to update the web...
            if (last_command == null){
                return;
            }
            var last_command_command = last_command.slice(0,3);  // Get first three letters
            var last_command_value = Number(last_command.slice(3));
            console.log('last command: ' + last_command_command + last_command_value);
            switch(last_command_command){
                case 'POW':
                    if (last_command_value == 0){
                        console.log('Setting State False and enabling transistion');
                        current_power = false;
                        power_in_transition = true;
                        volume_in_transition = true;
                        mute_in_transition = true;
                        update_talent(power_talent, {'state': false, 'in_transition': true})
                        update_talent(mute_talent, {'in_transition': true})
                        update_talent(volume_talent, {'in_transition': true})
                    }
                    if (last_command_value == 1){
                        console.log('Setting State True and enabling transistion');
                        current_power = true;
                        power_in_transition = true;
                        mute_in_transition = true;
                        volume_in_transition = true;
                        update_talent(power_talent, {'state': true, 'in_transition': true})
                        update_talent(mute_talent, {'in_transition': true})
                        update_talent(volume_talent, {'in_transition': true})
                    }
                    last_command = null
                    break;


                case 'MUT':
                    if (last_command_value == 0){
                        console.log('Setting Mute False');
                        current_mute = false;
                        if (mute_in_transition){
                            mute_in_transition = false;
                        }
                        // Make sure this is right!!!
                        update_talent(mute_talent, {'state': false, 'in_transition': false})

                    }
                    if (last_command_value == 1){
                        console.log('Setting Mute True');
                        current_mute = true;
                        if (mute_in_transition){
                            mute_in_transition = false;
                            update_talent(mute_talent, {'state': true, 'in_transition': false})
                        } else {
                            update_talent(mute_talent, {'state': true})
                        }
                    }
                    last_command = null;
                    break;

                case 'VOL':
                    console.log('Setting Volume range_value to ' + last_command_value);
                    current_volume = last_command_value;
                    if (volume_in_transition){
                        volume_in_transition = false;
                        update_talent(volume_talent, {'in_transition': false, "range_value": current_volume}) 
                    } else {
                        update_talent(volume_talent, {'range_value': current_volume})
                    }
                    last_command = null;
                    break;

                default:
                    last_command = null;
                    return;
            }
            break;

        case 'POW':
            // Power
            switch(data_value){
                case 0:  // power is off

                    // If current power is on, mark it off and clear the transition... if not double check that transistion has been cleared.
                    if(current_power){
                        console.log('Setting Power False');
                        current_power = false;
                        power_in_transition = false;
                        // Because we want to disable the mute and volume we need to leave them in transistion
                        mute_in_transition = true;
                        volume_in_transition = true;
                        update_talent(power_talent, {'state': false, 'in_transition': false})
                        update_talent(mute_talent, {'in_transition': true})
                        update_talent(volume_talent, {'in_transition': true})
                    } else {
                        // Clear transition
                        if (power_in_transition){
                            power_in_transition = false;
                            update_talent(power_talent, {'in_transition': false})
                            console.log('For some reason transition was not cleared correctly')
                        }
                        // Do not clear transistion for mute and volume when power is off!
                    }
                    break;

                case 1:  //power is on

                    // If current power is off, mark it on and clear the transition... if not double check that transistion has been cleared.
                    if (!current_power){
                        console.log('Setting Power True');
                        current_power = true;
                        power_in_transition = false;
                        update_talent(power_talent, {'state': true, 'in_transition': false})
                        update_talent(mute_talent, {'in_transition': false})
                        mute_in_transition = false;
                        update_talent(volume_talent, {'in_transition': false})
                        volume_in_transition = false;
                    } else {
                        // This is a check if transition hasn't be cleared
                        if (power_in_transition){
                            power_in_transition = false;
                            update_talent(power_talent, {'in_transition': false})
                        }
                        if (mute_in_transition){
                            mute_in_transition = false;
                            update_talent(mute_talent, {'in_transition': false})
                        }
                        if (volume_in_transition){
                            volume_in_transition = false;
                            update_talent(volume_talent, {'in_transition': false})
                        }
                    }
                    // Since we are on .. we can set any stored volume or mute
                    // We don't want to do this after an ACK, we need to wait for the project to be on.
                    if (volume_transition_value != null){
                        console.log('Setting transitional value for volume');
                        command_set_volume(volume_transition_value);
                        volume_transition_value = null;
                    }
                    if (mute_transistion_value != null){
                        console.log('Setting transitional value for mute');
                        command_set_mute(mute_transistion_value);
                        mute_transistion_value = null;
                    }
                    break;
                
                   
                case 2:
                case 3:
                    if (data_value == 2){
                        console.log('Projector warming');
                    } else {
                        console.log('Projector cooling');
                    }

                    if (!power_in_transition){
                        power_in_transition = true;
                        update_talent(power_talent, {'in_transition': true})
                        console.log('Power was changed outside of the program so we need to mark transition')
                    }
                    if (!mute_in_transition){
                        mute_in_transition = true;
                        update_talent(mute_talent, {'in_transition': true})
                        console.log('Power was changed outside of the program so we need to mark transition')
                    }
                    if (!volume_in_transition){
                        volume_in_transition = true;
                        update_talent(volume_talent, {'in_transition': true})
                        console.log('Power was changed outside of the program so we need to mark transition')
                    }
                    break;

                default:
                    console.log("No case for power value: " + data_value);
            };
            break;

        case 'VOL':
            // Volume
            // console.log('current vol: ' + current_volume)
            // console.log('data_value: ' + data_value)
            // console.log('current volume is not equal to data_value: ' + current_volume != data_value)
            if (current_volume != data_value){
                console.log('Updating volume to ' + data_value + " from current_volume: " + current_volume);
                current_volume = data_value; // Need to do this first, so we don't react to the update
                update_talent(volume_talent, {'range_value': data_value, 'requested_range_value': data_value, 'in_transition': !current_power})
            }
            break;

        case 'MUT':
            if (data_value == 0){
                // Mute off
                if (current_mute){
                    console.log('Setting Mute Off');
                    current_mute = false;
                    update_talent(mute_talent, {'state': false, 'in_transition': !current_power})
                }
            }
            if (data_value == 1){
                //Mute on
                if (!current_mute){
                    console.log('Setting Mute On');
                    current_mute = true;
                    update_talent(mute_talent, {'state': true, 'in_transition': !current_power})
                }
            }
            break;

        default:
            console.log("No case for data_command: " + data_command);
            return;
    }
});

function connect() {
    console.log("In TCP connect")
    client.connect({
        port: HOST_PORT,
        host: HOST_IP
    })
}
function launchTCPIntervalConnect() {
    console.log("In launchTCPIntervalConnect")
    if(false != intervalTCPConnect) return
    intervalTCPConnect = setInterval(connect, 5000)
}

function clearTCPIntervalConnect() {
    console.log("In clearTCPInterval")
    if(false == intervalTCPConnect) return
    clearInterval(intervalTCPConnect)
    intervalTCPConnect = false
}

client.on('connect', () => {
    clearTCPIntervalConnect()
    device_is_online = true;
    console.log('Connected');
    if (device_id != 0){
         send_ws_data({'stream': 'devicestream',
                       'payload': {'action': 'patch',
                                   'data': {'is_online': true}, 
                                   'request_id': 42,
                                   'pk': device_id}})
         }
    //get inital values
    get_pow();
    setTimeout(get_mute, 200);
    setTimeout(get_vol, 400);
});

client.on('error', (err) => {
    // logger(err.code, 'TCP ERROR')
    console.log('in tcp socket close')
    if (device_is_online){
      device_is_online = false;
      send_ws_data({'stream': 'devicestream',
                    'payload': {'action': 'patch',
                                'data': {'is_online': false}, 
                                'request_id': 42,
                                'pk': device_id}})
    }
    launchTCPIntervalConnect();
})
client.on('close', function(){
  console.log('in tcp socket close')
  if (device_is_online){
      device_is_online = false;
      send_ws_data({'stream': 'devicestream',
                    'payload': {'action': 'patch',
                                'data': {'is_online': false}, 
                                'request_id': 42,
                                'pk': device_id}})
  }
  launchTCPIntervalConnect();
})
client.on('end', function(){
    console.log('in TCP end?')
    if (device_is_online){
      device_is_online = false;
      send_ws_data({'stream': 'devicestream',
                    'payload': {'action': 'patch',
                                'data': {'is_online': false}, 
                                'request_id': 42,
                                'pk': device_id}})
   }
    launchTCPIntervalConnect()
})

///// Websocket code////

//connect to webserver


function launchWSIntervalConnect() {
    console.log('launchWSIntervalConnect called intervalWSConnect is ' + intervalWSConnect)
    if(false != intervalWSConnect) return
    //reset all values
    user_id = 0;
    driver_id = 0;
    device_id = 0;

    power_talent = 0;
    mute_talent = 0;
    volume_talent = 0;

    request_lookup = {'power': null, 'mute': null, 'volume': null}

    subscriptions = {'driverstream': new Set(), 'devicestream': new Set(), 'talentstream': new Set()}
    intervalWSConnect = setInterval(function(){
        ws = wsconnect()
    }, 5000)
}

function clearWSIntervalConnect() {
    console.log('clearWSIntervalConnect called intervalWSConnect is ' + intervalWSConnect)
    if(false == intervalWSConnect) return
    clearInterval(intervalWSConnect)
    intervalWSConnect = false
}

var ws = wsconnect()

function wsconnect() {
    console.log('wsconnect called intervalWSConnect is ' + intervalWSConnect)
    //Whenever we connect we need to make sure we get everything updated
    
    var ws = new WebSocket(SERVER_URL, {
        headers: {
            'Authorization': 'Token ' + token
       }
    });



    ws.on('close', function close(){
        console.log('Websocket is closed... intervalWSConnect is ' + intervalWSConnect)
        launchWSIntervalConnect()
    })

    ws.on('error', function error(){
        console.log('Websocket has had an error... intervalWSConnect is ' + intervalWSConnect)
        launchWSIntervalConnect()
    })


    ws.on('open', function open() {
        clearWSIntervalConnect()
        console.log('Websocket is open...')
        const watchdog = setInterval(() => {
            // Need to send it this way so we can include the driver
            if (driver_id != 0){
                ws.send(JSON.stringify({'stream': 'watchdogstream', 'payload': {'action': 'update_watchdog', 'data': {}, 'driver': driver_id, 'request_id': 42}}));
            }
        }, 20000);

       
        if (uuid == ''){
            // We need to get a uuid
            send_ws_data({'stream': 'driverstream',
                          'payload': {'action': 'create',
                                      'data': {'owner': {'id': user_id},
                                               'name': 'LightThrower Projector',
                                               'is_online': true,
                                               'devices': [],
                                               'configs': []},
                                               'request_id': 42}});
        } else {
            // We need to get our driver details
            send_ws_data({'stream': 'driverstream',
                          'payload': {'action': 'list',
                                      'data': {},
                                      'request_id': 42}});
        }


    });


    ws.on('message', function incoming(incoming_data) {
        // console.log(incoming_data);
        const all_data = JSON.parse(incoming_data);
        // console.log(data)
        // if (all_data.payload.action == 'patch'){
        //     if (all_data.payload.errors.length){
        //         console.log("Patch error: " + all_data.payload.errors)
        //     }
        //     return;
        // }
        ws_stream = all_data.stream
        ws_payload = all_data.payload
        if ('action' in ws_payload){
            ws_action = all_data.payload.action;
        };
        if ('errors' in ws_payload){
            if (ws_payload.errors.length > 0){
                   console.log("Errors detected: ");
                   console.log(all_data);
                   console.log(ws_payload.errors)
                   return
           }
       };

        switch(ws_stream) {
            case 'driverstream':
                driver = ws_payload.data
                switch(ws_payload.action){
                    case 'create':
                        uuid = driver.driver_uuid;
                        file_data = JSON.stringify({'uuid': uuid})
                        fs.writeFile('ltp_uuid.json', file_data, function (err) {
                          if (err) throw err;
                          console.log('Saved!');
                        });
                        console.log("Requesting drivers list");
                        send_ws_data({'stream': 'driverstream',
                                      'payload': {'action': 'list',
                                                  'data': {},
                                                  'request_id': 42}});
                        break;
                    case 'list':
                        // console.log(ws_payload);
                        found_our_uuid = false;
                        ws_payload.data.forEach(function(driver) {
                            if (driver.driver_uuid == uuid){
                                found_our_uuid = true;
                                driver_id = driver.id;
                                authorized = driver.authorized;
                                if (authorized){
                                    //We have verified our uuid and have our driver_id lets mark ourselves online
                                    ws.send(JSON.stringify({'stream': 'watchdogstream', 'payload': {'action': 'update_watchdog', 'data': {}, 'driver': driver_id, 'request_id': 42}}));
                                    console.log("Requesting devices list");
                                    send_ws_data({'stream': 'devicestream',
                                                  'payload': {'action': 'list',
                                                              'request_id': 42}})
                                    
                                } else {
                                    // retrieve 
                                    console.log("Initially retrieving my driver id:" + driver_id + " to check auth")
                                    send_ws_data({'stream': 'driverstream',
                                                  'payload': {'action': 'retrieve',
                                                              'pk': driver_id,
                                                              'request_id': 42}});
                                }
                                
                            }
                        })
                        if (!found_our_uuid){
                            //If we made it here, we haven't found our uuid on the server -- send a warning and shutdown
                            console.log("************** **  Our uuid wasn't found on the webserver! ** ****************")
                            console.log("This can happen if you are connected to the wrong server or if your driver was deleted from the server.")
                            console.log("If you are sure you are on the correct server, copy the correct uuid from the server into ltp_uuid.json")
                            console.log("Or delete ltp_uuid.json and allow the driver to re-register.")
                            process.exit(1)

                        }
                        
                        break;
                    case 'retrieve':
                        if (driver.driver_uuid == uuid){
                            driver_id = driver.id;
                            authorized = driver.authorized;
                            if (authorized){
                                    //We have verified our uuid and have our driver_id lets mark ourselves online
                                    ws.send(JSON.stringify({'stream': 'watchdogstream', 'payload': {'action': 'update_watchdog', 'data': {}, 'driver': driver_id, 'request_id': 42}}));
                                    console.log("Requesting devices list");
                                    send_ws_data({'stream': 'devicestream',
                                                  'payload': {'action': 'list',
                                                              'request_id': 42}})
                            } else {
                                // retrieve every 5 seconds
                                console.log("Awaiting authorization...")
                                const awaitauth = setTimeout(() => {
                                    console.log("Retrieving my driver id: " + driver_id + " and uuid: " + uuid + " to check auth")
                                    send_ws_data({'stream': 'driverstream',
                                                  'payload': {'action': 'retrieve',
                                                              'pk': driver_id,
                                                              'request_id': 42}});
                                    }, 5000);
                                
                            };
                        };     
                        break;
                    // case 'update':
                    // case 'delete':
                    //     break;
                    default:
                        console.log("Default case: ");
                        console.log(all_data);
                }
                break;
            case 'devicestream':
                device = ws_payload.data
                switch(ws_action){
                    case 'list':
                        found_existing_device = false;
                        ws_payload.data.forEach(function (device) {
                            if (device.unique_id == projector_unique_id){
                                // This is our projector
                                device_id = device.id;
                                found_existing_device = true;
                                // Subscribe to it
                                if (!subscriptions.devicestream.has(device_id)){
                                    console.log("Subscribing to device " + device_id)
                                    send_ws_data({'stream': 'devicestream',
                                                  'payload': {'action': 'subscribe_instance',
                                                              'pk': device.id,
                                                              'request_id': 42}})
                                    subscriptions.devicestream.add(device_id)
                                    console.log("Retrieving device to update status")
                                    send_ws_data({'stream': 'devicestream',
                                              'payload': {'action': 'retrieve',
                                                          'pk': device.id,
                                                          'request_id': 42}});
                                    console.log("Requesting talent list")
                                    send_ws_data({'stream': 'talentstream',
                                                  'payload': {'action': 'list',
                                                              'request_id': 42}})
                                }

                            }
                        });
                        if (!found_existing_device){
                            // Since we made it here without finding our projector, we need to create our projector device
                            console.log("Creating device")
                            send_ws_data({'stream': 'devicestream',
                                          'payload': {'action': 'create',
                                                      'data': {'name': 'LightThrowerProjector',
                                                               'description': 'This is the ltp device',
                                                               'unique_id': projector_unique_id},
                                                      'driver': uuid,
                                                      'request_id': 42}})
                        };
                        break;
                    case 'create':
                        if (!subscriptions.devicestream.has(device.id)){
                            //Set our device_id
                            device_id = device.id;
                            console.log("Subscribing to device " + device.id)
                            send_ws_data({'stream': 'devicestream',
                                          'payload': {'action': 'subscribe_instance',
                                                      'pk': device.id,
                                                      'request_id': 42}})
                            subscriptions.devicestream.add(device.id)
                            console.log("Retrieving device to update status")
                            send_ws_data({'stream': 'devicestream',
                                      'payload': {'action': 'retrieve',
                                                  'pk': device.id,
                                                  'request_id': 42}});
                            console.log("Requesting talents after creating device")
                            send_ws_data({'stream': 'talentstream',
                                          'payload': {'action': 'list',
                                                      'request_id': 42}})
                        }
                        break;
                    case 'subscribe_instance':
                        //console.log(all_data)
                        break;
                    case 'retrieve':
                        // We need to make sure the web matches our state
                        if (device.is_online != device_is_online){
                            send_ws_data({'stream': 'devicestream', 
                                          'payload': {'action': 'patch',
                                                      'data': {'is_online': device_is_online}, 
                                                      'request_id': 42,
                                                      'pk': device.id}});
                        }
                        break;
                    case 'delete':
                        // If we get here our device has been delete manually... I'll recreate it..
                        console.log("Creating device")
                        send_ws_data({'stream': 'devicestream',
                                      'payload': {'action': 'create',
                                                  'data': {'name': 'LightThrowerProjector',
                                                           'description': 'This is the ltp device',
                                                           'unique_id': projector_unique_id},
                                                  'driver': uuid,
                                                  'request_id': 42}})
                        break;
                    case 'patch':
                    case 'update':
                        break;
                    default:
                        console.log("Default case: ");
                        console.log(all_data);
                };
                break;
            case 'talentstream':
                talent = ws_payload.data
                switch(ws_action){
                    case 'list':
                        // console.log(all_data);
                        ws_payload.data.forEach(function(talent) {
                            // console.log(talent)
                            if (talent.device.id == device_id){
                                //we should subscribe to each talent
                                if (talent.name == 'Power'){
                                    power_talent = talent.id
                                    console.log("Subscribing to Power talent " + talent.id)
                                    subscribe_talent(talent.id)
                                    subscriptions.talentstream.add(talent.id)                 
                                    // Since we subscribed to a talent, lets retrieve it to update current status
                                    send_ws_data({'stream': 'talentstream',
                                                  'payload': {'action': 'retrieve',
                                                              'pk': talent.id,
                                                              'request_id': 42}});

                                };
                                if (talent.name == 'Mute'){
                                    mute_talent = talent.id
                                    console.log("Subscribing to Mute talent " + talent.id)
                                    subscribe_talent(talent.id)
                                    subscriptions.talentstream.add(talent.id)
                                    // Since we subscribed to a talent, lets retrieve it to update current status
                                    send_ws_data({'stream': 'talentstream',
                                                  'payload': {'action': 'retrieve',
                                                              'pk': talent.id,
                                                              'request_id': 42}});

                                };
                                if (talent.name == 'Volume'){
                                    volume_talent = talent.id
                                    console.log("Subscribing to Volume talent " + talent.id)
                                    subscribe_talent(talent.id)
                                    subscriptions.talentstream.add(talent.id)
                                    // Since we subscribed to a talent, lets retrieve it to update current status
                                    send_ws_data({'stream': 'talentstream',
                                                  'payload': {'action': 'retrieve',
                                                              'pk': talent.id,
                                                              'request_id': 42}});

                                };
                            };
                        });
                        // We need to check if the talents have been created, and create them if the driver has already been created
                        if (device_id == 0){
                            console.log('ERROR: Returning because we dont have a device_id yet this should never happen');
                            return;
                        };
                        if (power_talent == 0){
                            console.log("Creating power talent with device " + device_id)
                            send_ws_data({'stream': 'talentstream',
                                          'payload': {'action': 'create',
                                                      'data': {'name': 'Power',
                                                               'description': 'This turns the power on and off on the projector',
                                                               'state': false},
                                                      'device': device_id,
                                                      'request_id': current_request_id}});
                            request_lookup.power = current_request_id;
                            current_request_id += 1;
                        };
                        if (mute_talent == 0){
                            console.log("Creating mute talent")
                            send_ws_data({'stream': 'talentstream',
                                          'payload': {'action': 'create',
                                                      'data': {'name': 'Mute',
                                                               'description': 'This turns the mute on and off on the projector, it can only be controlled while the projector is on.',
                                                               'in_transition': true,
                                                               'disable_if_in_transition': true},
                                                      'device': device_id,
                                                      'request_id': current_request_id}});
                            request_lookup.mute = current_request_id;
                            current_request_id += 1;
                        };
                        if (volume_talent == 0){
                            console.log("Creating volume talent")
                            send_ws_data({'stream': 'talentstream',
                                          'payload': {'action': 'create',
                                                      'data': {'name': 'Volume',
                                                               'description': 'This sets the volume level of the projector. The minimum is 0 and max is 63.',
                                                               'is_range': true,
                                                               'range_value': 0,
                                                               'range_min': 0,
                                                               'range_max': 63,
                                                               'in_transition': true,
                                                               'disable_if_in_transition': true},
                                                      'device': device_id,
                                                      'request_id': current_request_id}});
                            request_lookup.volume = current_request_id;
                            current_request_id += 1;
                        };

                        break;

                    case 'create':
                        console.log("In talent create ws")
                        console.log(all_data)
                        console.log(request_lookup)
                        console.log(request_lookup.power)
                        console.log(ws_payload.request_id)
                        console.log(request_lookup.power == ws_payload.request_id)
                        if (ws_payload.request_id == request_lookup.power){
                            power_talent = talent.id;
                        }
                        if (ws_payload.request_id == request_lookup.mute){
                            mute_talent = talent.id;
                        }
                        if (ws_payload.request_id == request_lookup.volume){
                            volume_talent = talent.id;
                        }
                        console.log('power_talent: ' + power_talent)
                        console.log('mute_talent: ' + mute_talent)
                        console.log('volume_talent: ' + volume_talent)
                        if (!subscriptions.talentstream.has(talent.id)){
                            console.log("Subscribing to talent " + talent.id)
                            subscribe_talent(talent.id)
                            subscriptions.talentstream.add(talent.id)
                            // Since we subscribed to a talent, lets retrieve it to update current status
                            send_ws_data({'stream': 'talentstream',
                                          'payload': {'action': 'retrieve',
                                                      'pk': talent.id,
                                                      'request_id': 42}});
                        }

                        break;
                    case 'subscribe_instance':
                        break;

                    case 'retrieve':
                        //We need to make sure the web matches our current state
                        switch(talent.id){
                            case power_talent:
                                if (current_power == null || power_in_transition == null){
                                    // Since we don't know what the state of the projector is, lets set it to the web last known
                                    current_power = talent.state
                                    power_in_transition = talent.in_transition
                                } else {
                                send_ws_data({'stream': 'talentstream',
                                              'payload': {'action': 'patch',
                                                          'data': {'state': current_power,
                                                                   'in_transition': power_in_transition, 
                                                                   'set_on': false,
                                                                   'set_off': false},
                                                          'pk': talent.id,
                                                          'request_id': 42}});
                                }
                                break;
                            case mute_talent:
                                if (current_mute == null || mute_in_transition == null){
                                    // Since we don't know what the state of the projector is, lets set it to the web last known
                                    current_mute = talent.state
                                    // If the talent is in transistion or power is off mark it in transistion
                                    if (talent.in_transition || !current_power){
                                        mute_in_transition = true
                                    } else {
                                        mute_in_transition = false
                                    }
                                } else {
                                send_ws_data({'stream': 'talentstream',
                                              'payload': {'action': 'patch',
                                                          'data': {'state': current_mute,
                                                                   'in_transition': mute_in_transition, 
                                                                   'set_on': false,
                                                                   'set_off': false},
                                                          'pk': talent.id,
                                                          'request_id': 42}});
                                }
                                break;
                            case volume_talent:
                                if (current_volume == null || volume_in_transition == null){
                                    // Since we don't know what the state of the projector is, lets set it to the web last known
                                    current_volume = talent.range_value
                                    // If the talent is in transistion or power is off mark it in transistion
                                    if (volume_in_transition || !current_power){
                                        volume_in_transition = true
                                    } else {
                                        volume_in_transition = false
                                    }

                                } else {
                                send_ws_data({'stream': 'talentstream',
                                              'payload': {'action': 'patch',
                                                          'data': {'in_transition': volume_in_transition, 
                                                                   'range_value': current_volume,
                                                                   'range_requested_value': current_volume},
                                                          'pk': talent.id,
                                                          'request_id': 42}})
                                }
                                break;
                            default:
                                console.log('power_talent: ' + power_talent)
                                console.log('mute_talent: ' + mute_talent)
                                console.log('volume_talent: ' + volume_talent)
                                console.log("Default case: ");
                                console.log(all_data);

                        }
                        break;
                    case 'update':
                        console.log('Got update: ' + ws_stream + ' ' + talent.name)
                        if (talent.set_on){
                            switch(talent.id){
                                case power_talent:
                                    // We have a request to turn power on
                                    if (current_power){
                                        console.log('Skipping power on because we are already powered on')
                                    } else {
                                        command_set_power('001');
                                    }
                                    // Clear request
                                    update_talent(power_talent, {'set_on': false})
                                    break;
                                case mute_talent:
                                    // We have a request to turn mute on
                                    if (current_mute){
                                        console.log('Skipping mute on because we are already muted')
                                    } else {
                                        // We cannot mute when powered off or in transition
                                        if (mute_in_transition || !current_power){
                                            // We need to store the mute request
                                            mute_transistion_value = 'MUT001'
                                        } else {
                                            command_set_mute('001')
                                        }
                                    }
                                    // Clear request
                                    update_talent(mute_talent, {'set_on': false})
                                    break;
                                default:
                                    console.log("Default case: ");
                                    console.log(all_data);
                                    return;

                            }
                        };
                        if (talent.set_off){
                            switch(talent.id){
                                case power_talent:
                                    // We have a request to turn power off
                                    if (!current_power){
                                        console.log('Skipping power on because we are already powered on')
                                    } else {
                                        command_set_power('000');
                                    }
                                    // Clear request
                                    update_talent(power_talent, {'set_off': false})
                                    break;
                                case mute_talent:
                                    // We have a request to turn mute off
                                    if (!current_mute){
                                        console.log('Skipping mute off because we are not muted')
                                    } else {
                                        // We cannot mute when powered off or in transition
                                        if (mute_in_transition || !current_power){
                                            // We need to store the mute request
                                             mute_transistion_value = 'MUT000'
                                        } else {
                                            command_set_mute('000')
                                        }  
                                    }
                                    // Clear request
                                    update_talent(mute_talent, {'set_off': false})
                                    break;
                                default:
                                    console.log("Default case: ");
                                    console.log(all_data);
                                    return;
                            }
                        };
                        if (talent.in_transition){
                            switch(talent.id){
                                case power_talent:
                                    // The web server has transition checked, if we are not in transtion we need to clear it
                                    if (!power_in_transition){
                                        update_talent(power_talent, {'in_transition': false})
                                        console.log("Had to clear in transition unexpectedly")
                                    }
                                    break;
                                case mute_talent:
                                    // The web server has transition checked, if we are not in transtion we need to clear it
                                    if (!mute_in_transition){
                                        update_talent(mute_talent, {'in_transition': false})
                                        console.log("Had to clear in transition unexpectedly")
                                    }
                                    break;
                                case volume_talent:
                                    // The web server has transition checked, if we are not in transtion we need to clear it
                                    if (!mute_in_transition){
                                        update_talent(volume_talent, {'in_transition': false})
                                        console.log("Had to clear in transition unexpectedly")
                                    }
                                    break;
                                default:
                                    console.log("Default case: ");
                                    console.log(all_data);
                                    return;
                            }
                        };
                        if (talent.id == volume_talent){
                            // We need to check for a volume change request
                            if (talent.requested_range_value != current_volume){
                                // We need to change the volume
                                // We cannot change volume while powered down
                                if (volume_in_transition || !current_power){
                                    volume_transition_value = talent.requested_range_value;
                                } else {
                                    if (current_mute){
                                        // if they are turning the volume up, they must want to unmute right?!?
                                        if (talent.requested_range_value > current_volume){
                                            command_set_volume(talent.requested_range_value)
                                            // We need to delay this so the project can respond to volume
                                            setTimeout(function () {
                                                command_set_mute('000')
                                            }, 200);
                                            return;
                                        }
                                    }
                                    command_set_volume(talent.requested_range_value)
                                }
                            }
                        };
                        if (talent.state){
                            //Verify the state matches our values
                            switch(talent.id){
                                case power_talent:
                                    if(current_power != talent.state){
                                        console.log("We have a mismatched power state")
                                        if(talent.state){
                                            update_talent(power_talent, {'state': false})
                                        } else {
                                            update_talent(power_talent, {'state': true})
                                        }
                                    }
                                    break;
                                case mute_talent:
                                    if(current_mute != talent.state){
                                        console.log("We have a mismatched mute state")
                                        if(talent.state){
                                            update_talent(mute_talent, {'state': false})
                                        } else {
                                            update_talent(mute_talent, {'state': true})
                                        }
                                    }
                                    break;
                                default:
                                    console.log("Default case: ");
                                    console.log(all_data);
                                    return;
                            }
                        };
                        break;
                    case 'patch':
                        break;
                    default:
                        console.log("Default case: ");
                        console.log(all_data);
                };
            default:
                return;
        }
    });
    return ws
}

function send_ws_data(data){
    if (ws.readyState === WebSocket.OPEN) {
        console.log('Sending: ')
        console.log(data)
        ws.send(JSON.stringify({'stream': data.stream, 'payload': data.payload}))
    }
}

function subscribe_talent(talent_id){
    send_ws_data({'stream': 'talentstream',
                  'payload': {'action': 'subscribe_instance',
                              'pk': talent_id,
                              'request_id': 42}})
}


function update_talent(pk, data){
    if (pk == 0){
        console.log("Skipping talent update, as it hasn't been created yet")
        return;
    }
    send_ws_data({'stream': 'talentstream', 
                  'payload': {'action': 'patch',
                              'data': data, 
                              'request_id': 42,
                              'pk': pk}});
 };


function command_set_power(value){
  console.log('Commanding Power ' + value)
  client.write('POW' + value +'\r\n');
  last_command = 'POW' + value
}

function command_set_mute(value){
  console.log('Commanding Mute ' + value)
  client.write('MUT' + value + '\r\n');
  last_command = 'MUT' + value
}

function command_set_volume(value){
  console.log('Commanding Volume ' + value)
  client.write('VOL' + value + '\r\n');
  last_command = 'VOL' + value;
}

