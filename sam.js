const WebSocket = require('ws');

function base64Encode(aStr) {
    return Buffer.from(aStr).toString('base64');
};


var appname = base64Encode("SamsungTvRemote2");
var baseURL = "wss://10.0.0.38:8002/api/v2/channels/samsung.remote.control?name=" + appname


function getToken() {
    var uri = baseURL;
    console.log('URL: ' + uri)
    let ws = new WebSocket(uri, {
        rejectUnauthorized: false
    });

    ws.on('message', (response) => {
        console.log("SOCKET 1 RECEIVED: " + response);

        var data = JSON.parse(response);

        if (data.event === "ms.channel.connect") {
            console.log("YOUR TOKEN IS:" + data.data.token);
        }
    })
}

getToken();