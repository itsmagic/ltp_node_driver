const WebSocket = require('ws');

function base64Encode(aStr) {
    return Buffer.from(aStr).toString('base64');
};


var appname = base64Encode("SamsungTvRemote2");
var baseURL = "wss://10.0.0.38:8002/api/v2/channels/samsung.remote.control?name=" + appname
var token = 12317743;


function doStuff() {

    uri = baseURL+"&token="+token;
    console.log('URL: ' + uri)
    ws = new WebSocket(uri, {
        rejectUnauthorized: false
    });


    ws.on('message', (response) => {
        console.log("SOCKET 2 RECEIVED:" + response);

        var data = JSON.parse(response);


        var cmd = {
            method: 'ms.remote.control',
            params: {
                Cmd: 'Click',
                DataOfCmd: 'KEY_VOLUP',
                Option: 'false',
                TypeOfRemote: 'SendRemoteKey'
            }
        }
        var str = JSON.stringify(cmd);
        console.log('SOCKET 2 SEND:' + str);
        ws.send(str);

    });


}


doStuff();